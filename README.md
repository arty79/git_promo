## Нужно скачать и установить (ссылки для ОС Windows)

1. https://www.oracle.com/java/technologies/javase-jdk8-downloads.html
   скачать и установить специальный набор, состоящий из java-машины для вашей ОС, например, 
   8-й версии - JDK8, и классов необходимых для написания программы.
2. https://www.jetbrains.com/idea/download/download-thanks.html?platform=windows  
   установить IDE (редактор) для удобства работы с кодом и запуска программы, например, Intellij IDEA
3. https://github.com/git-for-windows/git/releases/download/v2.26.2.windows.1/Git-2.26.2-64-bit.exe 
   Установить GIT - систему контроля версий
4. https://bitbucket.org/arty79/git_promo/src/master/ - 
    клонировать репозиторий с программой
    
---

## Инструкция по работе с репозиторием после установки

1. Открыть Git Bash (правой клавишей мыши в любой удобной папке)
2. Клонировать репозиторий с программой - git clone https://arty79@bitbucket.org/arty79/git_promo.git 
3. Перейти  в папку проекта – cd git_promo 
4. Переключиться на свою ветку – git checkout –b ivan_kuznetsov (меняем на свою фамилию и имя)
5. Создать в папке проекта подпапку src – mkdir src, и перейти в нее – cd src, а в ней подпапку com, и в ней подпапку company
6. В последней подпапке company создадим файл Main.java – touch Main.java, – и добавим его в индекс гита – git add Main.java
7. Сделать коммит – git commit –m “First commit”
8. Запушить в удаленный репозиторий новую ветку – git push origin ivan_kuznetsov